import React from 'react';
import Header from './components/Header/Header';
import Hero from './components/Hero/Hero';
import Informative from './components/Informative/Informative';
import Product from './components/Product/Product';
import News from './components/News/News';
import Map from './components/Map/Map';
import Footer from './components/Footer/Footer';

const App = () => {
  return (
    <>
      <Header />
      <Hero />
      <Informative />
      <Product />
      <News />
      <Map />
      <Footer />
    </>
  );
};

export default App;
