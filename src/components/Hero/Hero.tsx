import React from 'react';
import './Hero.scss';
import audio from '../../assets/images/audio.svg';

const Hero = () => {
  return (
    <section className="hero">
      <div className="container hero__container">
        <div className="hero__bg">
          <div className="row m-0">
            <div className="col-md-5 col-sm-7 col-xs-10 col-xs-offset-1 p-0">
              <div className="hero__content">
                <h1>Guļbūvju ražošana īsti Skandināviskās tradīcijās</h1>
                <button type="button" className="hero__audio-btn">
                  <img src={audio} alt="audio-mute" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
