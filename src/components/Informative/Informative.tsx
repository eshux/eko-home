import React from 'react';
import './Informative.scss';
import ruler from '../../assets/images/lineala-raksts.svg';

const Informative = () => {
  return (
    <section className="informative">
      <div className="container container-fluid">
        <div className="row center-xs informative__row">
          <div className="col-lg-3 col-sm-6 col-xs-12 p-0">
            <h4>RAŽOŠANA</h4>
            <div className="informative__subtitle">DIGITĀLA</div>
            <p>ietver tradīciju un kultūrvēsturiskā mantojuma glabāšanu</p>
          </div>
          <div className="col-lg-3 col-sm-6 col-xs-12 p-0">
            <h4>TEHNOLOĢIJA UN PIEREDZE</h4>
            <div className="flex">
              <div className="width-100">
                <div className="informative__subtitle">CAD CAM</div>
                <p>unikāla šīs metodes adaptācija</p>
              </div>
              <div className="width-100">
                <div className="informative__subtitle">20 gadi</div>
                <p>un 17 gadu pieredze ar CNC iekārtām</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 col-xs-12 p-0">
            <h4>APJOMI UN KAPACITĀTE</h4>
            <div className="flex">
              <div className="width-100">
                <div className="informative__subtitle">670+</div>
                <p>piegādātas guļbūves</p>
              </div>
              <div className="width-100">
                <div className="informative__subtitle">
                  30 000m<sup>2</sup>
                </div>
                <p>ražošanas telpu platība</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 col-xs-12">
            <h4>KVALITĀTE UN GARANTIJA</h4>
            <div className="flex">
              <div className="width-100">
                <div className="informative__subtitle">0,5mm</div>
                <p>apstrādes precizitāte</p>
              </div>
              <div className="width-100">
                <div className="informative__subtitle">98%</div>
                <p>eksports. Ražots Latvijā.</p>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-xs-12">
            <img src={ruler} alt="" className="informative__ruler-img" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Informative;
