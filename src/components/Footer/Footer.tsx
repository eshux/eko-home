import React from 'react';
import './Footer.scss';
import logo from '../../assets/images/logo-footer.svg';
import ruler from '../../assets/images/lineals-footer.svg';
import logo_caballero from '../../assets/images/caballero-logo.svg';
import fb from '../../assets/images/facebook.svg';
import li from '../../assets/images/linkedin.svg';
import yt from '../../assets/images/youtube.svg';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container footer__container">
        <div className="row">
          <div className="col-xs-12 p-0">
            <div className="footer__nav">
              <img src={logo} alt="logo" className="footer__logo" />
              <div className="footer__nav-content">
                <a href="/#downloads">Lejupielādes</a>
                <a href="/#terms">Sīkdatnes noteikumi</a>
                <a href="/#privacypolicy">Privātuma politika</a>
              </div>
            </div>
            <img src={ruler} alt="" className="footer__image" />
          </div>
        </div>
        <div className="row middle-xs center-xs">
          <div className="col-sm-4 col-xs-12 p-0">
            <p className="footer__text">
              @ 2020 EKO NAMS. Visas tiesības aizsargātas
            </p>
          </div>
          <div className="col-sm-4 col-xs-12 first-xs last-sm p-0">
            <div className="footer__caba-logo">
              <img src={logo_caballero} alt="caballero logo" />
            </div>
          </div>
          <div className="col-sm-4 col-xs-12 first-xs last-sm p-0">
            <div className="footer__socials">
              <a href="/#facebook">
                <img src={fb} alt="facebook" className="footer__socials-icon" />
              </a>
              <a href="/#linkedin">
                <img src={li} alt="linkedin" className="footer__socials-icon" />
              </a>
              <a href="/#youtube">
                <img src={yt} alt="youtube" className="footer__socials-icon" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
