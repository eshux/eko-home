import React from 'react';
import './Product.scss';
import product from '../../assets/images/product.jpg';

const Product = () => {
  return (
    <section className="product">
      <div className="container">
        <div className="row middle-xs">
          <div className="col-sm-5 col-xs-10 col-xs-offset-1 p-0">
            <div className="product__content">
              <h2>Produkts</h2>
              <p>
                Guļbūve ir viena no senākajām ēku konstrukcijām, kura,
                nezaudējot savu īpašo vērtību un nozīmi, saglabājusies līdz pat
                mūsdienām. Aizvien vairāk pilsētnieku izjūt nepieciešamību
                dzīvot ekoloģiski tīrā un mājīgā vidē, būt harmonijā ar dabu, ar
                sevi. Meistarīgi veidota guļbūve apvienojumā ar modernām
                tehnoloģijām palīdz šos sapņus realizēt dzīvē.
              </p>
              <a href="#more">
                Vairāk par produktu
                <i className="material-icons">arrow_right_alt</i>
              </a>
            </div>
          </div>
          <div className="col-sm-5 col-xs-10 col-sm-offset-0 col-xs-offset-1 p-0">
            <div className="product__content">
              <img src={product} alt="product" className="product__image" />
            </div>
          </div>
          <div className="col-sm-1 p-0">
            <div className="product__content">
              <div className="product__bg" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Product;
