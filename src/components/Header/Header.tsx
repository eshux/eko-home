import React, { useState } from 'react';
import './Header.scss';
import logo from '../../assets/images/logo.png';

const Header = () => {
  const [showMenu, setShowMenu] = useState(false);

  return (
    <header className="header">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className="header__content">
              <div className={showMenu ? 'logo-bg active' : 'logo-bg'}>
                <img src={logo} alt="logo" className="logo-image" />
              </div>
              <nav className={showMenu ? 'nav active' : 'nav'}>
                <div className="nav__wrapper">
                  <a href="/#kvalitate">Kvalitāte</a>
                  <a href="/#projekti">Projekti</a>
                  <div className="nav__dropdown">
                    <a href="/#">Sadarbība</a>
                    <div className="nav__dropdown-content">
                      <a href="/#1">Shēma</a>
                      <a href="/#2">Partneri</a>
                      <a href="/#3">
                        Lorem, ipsum dolor sit amet consectetur adipisicing
                        elit.
                      </a>
                    </div>
                  </div>
                  <a href="/#blogs">Blogs</a>
                  <a href="/#parmums">Par mums</a>
                  <a href="/#kontakti">Kontakti</a>
                </div>
              </nav>
              <div
                className={
                  showMenu
                    ? 'nav__dropdown nav__dropdown--lang active'
                    : 'nav__dropdown nav__dropdown--lang'
                }
              >
                <a href="/#">EN</a>
                <div className="nav__dropdown-content nav__dropdown-content--lang">
                  <a href="/#">NO</a>
                  <a href="/#">SE</a>
                  <a href="/#">RU</a>
                  <a href="/#">LV</a>
                </div>
              </div>
              <button
                type="button"
                onClick={() => setShowMenu(!showMenu)}
                className={showMenu ? 'menu active' : 'menu'}
              >
                <div
                  className={showMenu ? 'menu__line active' : 'menu__line'}
                />
                <div
                  className={showMenu ? 'menu__line active' : 'menu__line'}
                />
                <div
                  className={showMenu ? 'menu__line active' : 'menu__line'}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
