import React, { FC } from 'react';
import './Map.scss';
import map from '../../assets/images/map.png';

const Map: FC = () => {
  return (
    <section className="map">
      <div className="container map__container">
        <div className="row">
          <div className="col-xs-12 p-0">
            <h2>Produkcijas realizācija Skandināvijā</h2>
            <img src={map} alt="map" className="map__image" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Map;
