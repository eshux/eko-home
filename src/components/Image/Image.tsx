import React, { FC } from 'react';
import './Image.scss';

type Props = {
  src: string;
  name: string;
  text: string;
};

const Image: FC<Props> = ({ src, name, text }) => {
  return (
    <div className="image__wrapper">
      <img src={src} alt={name} className="image" />
      <div className="image__overlay" />
      <p className="image__text">{text}</p>
    </div>
  );
};

export default Image;
