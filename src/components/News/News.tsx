import React from 'react';
import './News.scss';
import Image from '../Image/Image';

const News = () => {
  return (
    <section className="news">
      <div className="news__bg" />
      <div className="container container-fluid">
        <div className="row center-xs">
          <div className="col-xs-12">
            <h2>Aktualitātes</h2>
          </div>
        </div>
        <div className="row news__content">
          <div className="col-sm-6 col-xs-12 p-0">
            <Image 
              text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." 
              src="https://i.pinimg.com/originals/12/98/cb/1298cb28af444b922a2f410df4b1a3ea.jpg"
              name="cabin" 
            />
          </div>
          <div className="col-sm-6 col-xs-12 p-0">
            <Image 
              text="Lorem Ipsum is simply dummy text of the printing and typesetting industry." 
              src="https://st.hzcdn.com/simgs/pictures/kuechen/bergdorf-liebesgruen-archifaktur-lennestadt-gmbh-img~3ab11c6d06f79696_4-1121-1-a85d7c5.jpg"
              name="cabin" 
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default News;
